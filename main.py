import base64
import datetime
import io

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import plotly.express as px

from config import suggested_tags
from evaluate import make_predictions

import pandas as pd

from dataengineer import create_data_as_columns

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server

app.layout = html.Div([
    html.H1(
        children='Savegus',
        style={
            'textAlign': 'center'
        }
    ),
    dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True
    ),
    html.Div(id='output-data-upload'),
])


def parse_contents(list_of_contents, list_of_names, list_of_dates):
    results = {}
    results_group_by_keys = suggested_tags
    results_group_by_values = [0] * len(suggested_tags)
    results_group_by = dict(zip(results_group_by_keys, results_group_by_values))

    for contents, filename, date in zip(list_of_contents, list_of_names, list_of_dates):
        content_type, content_string = contents.split(',')

        decoded = base64.b64decode(content_string)
        try:
            if 'csv' in filename:
                # Assume that the user uploaded a CSV file
                df = pd.read_csv(
                    io.StringIO(decoded.decode('utf-8')))
                column_df = create_data_as_columns(df, filename)
                prediction_tag =  make_predictions(column_df['columns_name'])
                results[filename] = prediction_tag
                results_group_by[prediction_tag] += 1

                # if prediction_tag in results:
                #     results[prediction_tag].append(filename)
                # else:
                #     results[prediction_tag] = [filename]

        except Exception as e:
            print(e)
            return html.Div([
                'There was an error processing this file.'
            ])
    # for key, value in results.items():
    #     results[key] = ' '.join(value)
    results_df = pd.DataFrame(results.items(), columns=['File Name', 'Tag'])
    results_group_by_df = pd.DataFrame(results_group_by.items(), columns=['Tag', 'Count'])

    fig = px.pie(results_group_by_df, values='Count', names='Tag')

    return html.Div([
        #html.H5(filename),
        #html.H6(datetime.datetime.fromtimestamp(date)),

        dash_table.DataTable(
            data=results_df.to_dict('records'),
            columns=[{'name': i, 'id': i} for i in results_df.columns],
            filter_action='native',
            style_table={
                'height': 400,
            },
            style_cell={'textAlign': 'left'},
            style_data={
                'width': '150px', 'minWidth': '150px', 'maxWidth': '150px',
                'overflow': 'hidden',
                'textOverflow': 'ellipsis',
            },
            style_cell_conditional=[
                {
                    'if': {'column_id': 'Region'},
                    'textAlign': 'left'
                }
            ]
        ),
        dcc.Graph(
            figure=fig
        ),
        html.Hr(),  # horizontal line

        # For debugging, display the raw contents provided by the web browser
        # html.Div('Raw Content'),
        # html.Pre(contents[0:200] + '...', style={
        #     'whiteSpace': 'pre-wrap',
        #     'wordBreak': 'break-all'
        # })
    ])


@app.callback(Output('output-data-upload', 'children'),
              [Input('upload-data', 'contents')],
              [State('upload-data', 'filename'),
               State('upload-data', 'last_modified')])
def update_output(list_of_contents, list_of_names, list_of_dates):
    if list_of_contents is not None:
        children = [
            parse_contents(list_of_contents, list_of_names, list_of_dates)]
        return children



if __name__ == '__main__':
    app.run_server(debug=True)
    #app.run_server(host='127.0.0.1', port=8080, debug=True)