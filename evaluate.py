import tensorflow as tf
import pickle
import numpy as np


from config import max_words, suggested_tags


def make_predictions(columns_df):
    model = create_model()
    model.load_weights('./checkpoints/my_checkpoint')

    with open('tokenizer.pickle', 'rb') as handle:
        tokenize = pickle.load(handle)

    columns_vector = tokenize.texts_to_matrix(columns_df)

    prediction = model.predict(columns_vector)
    predicted_label = suggested_tags[np.argmax(prediction)]
    return predicted_label

def create_model():
    # Build the model
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(512, input_shape=(max_words,)))
    model.add(tf.keras.layers.ReLU())
    model.add(tf.keras.layers.Dropout(0.5))

    model.add(tf.keras.layers.Dense(256, input_shape=(max_words,)))
    model.add(tf.keras.layers.ReLU())
    model.add(tf.keras.layers.Dense(128, input_shape=(max_words,)))
    model.add(tf.keras.layers.ReLU())

    model.add(tf.keras.layers.Dense(6, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model