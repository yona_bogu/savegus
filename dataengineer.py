import pandas as pd
import uuid
import re
from string import punctuation

# Array of all the unnecessary word in our data
problematic_columns_names = ['unnamed']

def format_columns(columns_names):
  columns_names_formatted = [str(name) for name in columns_names]
  columns_names_formatted = [name.lower() for name in columns_names_formatted]

  columns_names_formatted = [name if name.isdigit() else name.lower() for name in columns_names_formatted]
  # Deleting punctions
  columns_names_formatted = ' '.join(columns_names_formatted)
  r = re.compile(r'[\s{}]+'.format(re.escape(punctuation)))
  columns_names_formatted = r.split(columns_names_formatted)

  # Deleting unnecessary words or digits
  for problematic_column_name in problematic_columns_names:
    columns_names_formatted = [column_name for column_name in columns_names_formatted if (problematic_column_name not in column_name and (not column_name.isdigit()))]
  return columns_names_formatted


def create_data_as_columns(df_data,filename):
    column_data_df = pd.DataFrame(columns=['uuid', 'file_name', 'columns_name'])

    try:
        data_regular = {}
        columns_names_formatted = format_columns(df_data.columns.tolist())
        if len(columns_names_formatted) > 0:
            columns_names_formatted = ' '.join(columns_names_formatted)
            unique_id = uuid.uuid1()
            data_regular['uuid'] = str(unique_id)
            data_regular['file_name'] = filename
            data_regular['columns_name'] = columns_names_formatted
            df = pd.DataFrame(data_regular, index=[0])
            column_data_df = pd.concat([column_data_df, df], ignore_index=True)
    except Exception as e:
        print(e)

    return column_data_df